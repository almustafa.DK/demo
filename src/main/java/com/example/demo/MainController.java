package com.example.demo;


import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MainController {
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> greeting() {
        Map<String,String> map=new HashMap<>();
        map.put("message","welcome cok");
        map.put("statusCode","0000");
        return ResponseEntity.ok(map);
    }
}


